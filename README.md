# API Informe Legal

Este serviço disponibiliza acesso às informações relativas à situação e tramitação dos processos de Primeiro e Segundo Grau.

## Synopsis

At the top of the file there should be a short introduction and/ or overview that explains **what** the project is. This description should match descriptions added for package managers (Gemspec, package.json, etc.)

## Code Example

Show what the library does as concisely as possible, developers should be able to figure out **how** your project solves their problem by looking at the code example. Make sure the API you are showing off is obvious, and that your code is short and concise.

## Motivation

A short description of the motivation behind the creation and maintenance of the project. This should explain **why** the project exists.

## Installation

Provide code examples and explanations of how to get the project.

## API Reference

Depending on the size of the project, if it is small and simple enough the reference docs can be added to the README. For medium size to larger projects it is important to at least provide a link to where the API reference docs live.

## Tests

Describe and show how to run the tests with code examples.

## Contributors

Let people know how they can dive into the project, include important links to things like issue trackers, irc, twitter accounts if applicable.

## License

A short snippet describing the license (MIT, Apache, etc.)

## Installation and Running the App

* Clone this repo: `git clone https://pauljoseph@bitbucket.org/paulkibejoseph/judiciario-informe-legal-api.git`
* Access the folder: `cd judiciario-informe-legal-api`
* Install dependencies: `npm install`
* Boot the server: `npm start`
* Running tests: `npm test`
* The app will be served at `localhost:3000`.

## About

Informe Legal - [informe.legal.com.br](http://informe.legal.com.br)

## Request URL
> https://esaj.tjsp.jus.br/cpopg/search.do;jsessionid=8A0823A0B879C72E12A82E402A4390F0.cpopg10?conversationId=&dadosConsulta.localPesquisa.cdLocal=91&cbPesquisa=NUMPROC&dadosConsulta.tipoNuProcesso=UNIFICADO&numeroDigitoAnoUnificado=1223445-55.5666&foroNumeroUnificado=6666&dadosConsulta.valorConsultaNuUnificado=1223445-55.5666.8.26.6666&dadosConsulta.valorConsulta=&uuidCaptcha=

## Query String Parameters
- conversationId:
- dadosConsulta.localPesquisa.cdLocal:`91`
- cbPesquisa:`NUMPROC`
- dadosConsulta.tipoNuProcesso:`UNIFICADO`
- numeroDigitoAnoUnificado:`1223445-55.5666`
- foroNumeroUnificado:`6666`
- dadosConsulta.valorConsultaNuUnificado:`1223445-55.5666.8.26.6666`
- dadosConsulta.valorConsulta:
- uuidCaptcha:
