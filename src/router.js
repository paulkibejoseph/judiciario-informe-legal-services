const express = require("express");
const router = express.Router();

router.get("/", function(req, res, next) {
  res.status(200).json({
    aplicativo: "Informe Legal",
    descricao:
      "Este serviço disponibiliza acesso às informações relativas à situação e tramitação dos processos dos estados.",
    versao: "1.0.0"
  });
});

module.exports = router;
