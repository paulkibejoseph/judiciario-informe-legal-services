// const serviceAccount = require("../ServiceAccountKey.json");
const admin = require("firebase-admin");
const pubSub = require("@google-cloud/pubsub");
const _ = require("lodash");

// Init gcp services
// credential: admin.credential.cert(serviceAccount),
admin.initializeApp({
  credential: admin.credential.applicationDefault(),
  databaseURL: "https://informe-legal.firebaseio.com"
});
const firestore = admin.firestore();
const pubsub = new pubSub();

const FORMATS = {
  Processo: 'XXXXXXX-XX.XXXX.XXX.XXXX'
}

function atualizarProcesso(newProcesso, oldProcesso, filtro) {
  const numProcesso = _.get(newProcesso, "numProcesso");
  if (numProcesso && oldProcesso && filtro) {
    const update = {
      key: numProcesso,
      numProcesso: numProcesso,
      tribunal: { key: _.get(filtro, "tribunal"), instancia: _.get(filtro, "instancia"), secao: _.get(filtro, "secao") },
      partesPrincipais: _.get(newProcesso, "partesPrincipais"), 
      classe: _.get(newProcesso, "classe"), 
      dataUltimaMovimentacao: _.get(newProcesso, "todasMovimentacoes.[0].dado"),
      ultimaMovimentacao: _.get(newProcesso, "todasMovimentacoes.[0].valor")
    };
    console.log("atualizarProcesso-update", update);
    if (update.dataUltimaMovimentacao && update.ultimaMovimentacao) {
      return getUsers()
        .then(users => {
          const tasks = [];
          console.log("atualizarProcesso-users-length", users && users.length);
          users.forEach(user => 
            tasks.push(atualizarProcessoPorUsuario(update, user, newProcesso, oldProcesso))
          );
          return Promise.all(tasks)
                    .then(resp => {
                      console.log("atualizarProcessoPorUsuarios-resp", resp);
                      return resp;
                    })
                    .catch(error => {
                      console.error("atualizarProcessoPorUsuarios-error", error);
                      return error;
                    });
        });
    }
  }
  return Promise.reject({newProcesso, oldProcesso, filtro})
}

function atualizarProcessoPorUsuario(update, user, newProcesso, oldProcesso) {
  const pathFavorito = `/favoritos/${user.uid}/${update.numProcesso}`;
  const pathNotificacao = `/notificacoes/${user.uid}/${update.numProcesso}`;
  const pathFcmTokens = `/usuarios/${user.uid}/fcmTokens`;
  const getFavorito = admin.database().ref(pathFavorito).once('value');
  const getNotificacao = admin.database().ref(pathNotificacao).once('value');
  const getFcmTokens = admin.database().ref(pathFcmTokens).once('value');
  console.log("atualizarProcessoPorUsuario-pathFavorito", pathFavorito);
  console.log("atualizarProcessoPorUsuario-pathNotificacao", pathNotificacao);
  console.log("atualizarProcessoPorUsuario-pathFcmTokens", pathFcmTokens);
  return Promise.all([getFavorito, getNotificacao, getFcmTokens])
        .then(snapshot => {
          const favorito = snapshot[0].val();
          const notificacao = snapshot[1].val();
          const fcmTokens = snapshot[2].val();
          const updates = {};
          const notification = { tokens: [], payload: {} };
          console.log("atualizarProcessoPorUsuario-getFavorito", favorito);
          console.log("atualizarProcessoPorUsuario-getNotificacao", notificacao);
          console.log("atualizarProcessoPorUsuario-getFcmTokens", fcmTokens);
          if (favorito) {
            updates[`${pathFavorito}/dataUltimaMovimentacao`] = update.dataUltimaMovimentacao; 
            updates[`${pathFavorito}/ultimaMovimentacao`] = update.ultimaMovimentacao;
          }
          // && !_.isEqual(update.dataUltimaMovimentacao, _.get(oldProcesso, "todasMovimentacoes.[0].dado"))
          if (favorito && _.get(favorito, "favorito", false)) {
            if (fcmTokens) {
              notification.tokens = Object.keys(fcmTokens);
              notification.payload = {
                notification: {
                  title: "Novas atualizações disponíveis",
                  body: `O processo ${update.numProcesso} - ${update.ultimaMovimentacao}`,
                  icon: 'https://firebasestorage.googleapis.com/v0/b/informe-legal.appspot.com/o/assets%2Fimg%2Flogo.png?alt=media&token=7e64b7c7-1256-4e09-835b-3488568dc44a',
                  click_action: "https://dev-informe-legal.firebaseapp.com/#/principal/notificacao/notificacao"
                }
              }
            }
            updates[pathNotificacao] = Object.assign({}, update, { status: false}); 
          } else if (notificacao) {
            updates[pathNotificacao] = Object.assign({}, update, { status: true}); 
          }
          return { updates, notification };
        })
        .then(data => {
          console.log("atualizarProcessoPorUsuario-database-updates-and-messaging-data", data);
          const tasks = [];
          const updates = data['updates'];
          const notification = data['notification'];
          if (updates && Object.keys(updates).length > 0) {
              console.log("atualizarProcessoPorUsuario-database-updates", updates);
              tasks.push(admin.database().ref().update(updates));
          }
          if (notification && notification.tokens.length > 0 && Object.keys(notification.payload).length > 0) {
              console.log("atualizarProcessoPorUsuario-messaging-notification", notification)
              tasks.push(enfileirarPushNotification(notification.tokens, notification.payload));
          }
          return Promise.all(tasks)
            .then(resp => {
              console.log("atualizarProcessoPorUsuario-database-updates-and-messaging-resp", resp);
              return resp;
            })
            .catch(error => {
              console.error("atualizarProcessoPorUsuario-database-updates-and-messaging-error", error);
              return error;
            });
        })
        .catch(error => {
          console.error("atualizarProcessoPorUsuario-error", error);
          return error;
        });
}

function enfileirarPushNotification(tokens, payload) {
  const notification = { tokens, payload };
  console.log("enfileirarPushNotification-notification", notification);
  return pubsub.topic("enviar-push-notifications")
        .publisher()
        .publish(Buffer.from(JSON.stringify(notification)))
        .then(results => {
          const messageId = results;
          console.log("enfileirarPushNotification-resp", `Mensagem ${messageId} foi enfileirada.` );
          return messageId;
        })
        .catch(error => {
          console.error("enfileirarPushNotification-error", error);
          return error;
        });
}

function getUsers(users = [], nextPageToken) {
  return admin.auth().listUsers(1000, nextPageToken)
    .then((result) => {
      const list = [...users, ...result.users];
      if (result.pageToken) {
        return getUsers(list, result.pageToken);
      }
      return list;
    })
    .catch(error => {
      console.error("getUsers-error", error);
      return error;
    });
}

function formatter(source, format) {
  let regex = "";
  let newFormat = "";
  for (let i = 1; format.indexOf("X") >= 0; ++i) {
    newFormat = format.replace("X", "$" + i);
    regex += "(\\d)";
  }
  regex += "[^]*";
  return source.replace(new RegExp(regex), newFormat);
}

module.exports = {
  atualizarProcesso
};
