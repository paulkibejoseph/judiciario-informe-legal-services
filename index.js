"use strict";

const express = require("express");
const bodyParser = require("body-parser");
const compression = require("compression");
const cors = require("cors");
const cookieParser = require("cookie-parser")();
const _ = require("lodash");
const logging = require("./src/logging");

const app = express();
const HOSTNAME = "localhost";
const PORT = process.env.PORT || 8080;

app.set("json spaces", 4);

app.use(compression());
app.use(cors());
app.use(cookieParser);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(logging.requestLogger);

const middlewares = require("./src/middlewares");
const router = require("./src/router");

app.use("/", router);

app.post("/atualizarProcesso", (req, res) => {
  const body = req.body;
  console.log("atualizarProcesso-body", Object.keys(body));
  const filtro = body["filtro"];
  const newProcesso = body["newProcesso"];
  const oldProcesso = body["oldProcesso"];

  console.log("atualizarProcesso-filtro", Object.keys(filtro));
  console.log("atualizarProcesso-newProcesso", Object.keys(newProcesso));
  console.log("atualizarProcesso-oldProcesso", Object.keys(oldProcesso));

  middlewares
    .atualizarProcesso(newProcesso, oldProcesso, filtro)
    .then(resp => {
      console.log("atualizarProcesso-resp", resp);
      res.status(200).json(resp);
    })
    .catch(error => {
      console.error("atualizarProcesso-error", error);
      res.status(500).json(error);
    });
});

app.use(logging.errorLogger);

const server = app.listen(PORT, () => {
  console.log(`Server running at http://${HOSTNAME}:${PORT}/`);
});

module.exports = app;
